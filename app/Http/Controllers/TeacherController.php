<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    public function index()
    {
        return view('teacher.index');//blade index vo teacher folder
    }
    //===================== allData - site podatoci od baza / Ajax 1.===============//
    public function allData()
    {
        // $data = Teacher::orderBy('id', 'DESC')->get();
        $data = Teacher::all();
        return response()->json($data);
    }
    //====================  storeData / ajax 2===================================
    public function storeData(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'title' => 'required',
            'institute' => 'required',
        ]);
        $data = Teacher::insert([
            'name' => $request->name,
            'title' => $request->title,
            'institute' => $request->institute,
        ]);
        return response()->json($data);
    }
    // ------------------ Edit teacher data ---------------------------
    public function editData($id)
    {
        //    $data = Teacher::find($id);
       $data = Teacher::findOrFail($id);
       return response()->json($data);
    }
    //------------------- Update Data ---------------------------------
    public function updateData(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'title' => 'required',
            'institute' => 'required',
        ]);
        $data = Teacher::findOrFail($id)->update([
            'name' => $request->name,
            'title' => $request->title,
            'institute' => $request->institute,
        ]);
        return response()->json($data);
    }
    // ======================== Delete Data =============================
    public function destroyData(Request $request,$id){
        $data = Teacher::find($id)->delete();
        return response()->json($data);
    }
}
