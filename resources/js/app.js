const { default: Swal } = require('sweetalert2');

require('./bootstrap');

$('#addT').show();
$('#addButton').show();
$('#updateT').hide();
$('#updateButton').hide();


$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})
// =====================================================================================================
// =======================  1.ajax /  Zemanje na podatocite od baza   ========================================
// =======================================================================================================
function allData() {
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: "/teacher/all",
        success: function (response) {
            // var response = ""
            $.each(response, function (key, teacher) {

                $('#teacher-list tbody').prepend(`
          <tr>
                    <td>${teacher.id}</td>
                    <td>${teacher.name}</td>
                    <td>${teacher.title}</td>
                    <td>${teacher.institute}</td>
                    <td>
                    <button class="btn-sm btn-success edit-teacher-btn" data-teacher-id="${teacher.id}">Edit</button>
                    <button class="btn-sm btn-danger delete-teacher-btn" data-teacher-id="${teacher.id}">Delete</button>
                    </td>
         </tr>
                ` )
            })
        }
    })
}
allData();

// =======================================================================================
// ======================== 2. ajax / Add new teacher ===========================================
// =======================================================================================

function clearData() {
    $('#id').val('');
    $('#name').val('');
    $('#title').val('');
    $('#institute').val('');
    $('#nameError').text('')
    $('#titleError').text('')
    $('#instituteError').text('')
}
clearData()

$('#addButton').on('click', function (e) {
    var name = $('#name').val();
    var title = $('#title').val();
    var institute = $('#institute').val();

    $.ajax({
        type: "POST",
        dataType: "json",
        data: { name: name, title: title, institute: institute },
        url: "/teacher/store/",
        success: function (data) {
            clearData();
            $('#teacher-list tr').remove()
            allData();
            // Swal
            const Msg = Swal.mixin({
                position: 'top-end',
                toast: true,
                icon: 'success',
                showConfirmButton: false,
                timer: 3000
            })
            Msg.fire({
                // type:'success',
                title: 'Uspeshno dodaden nov user'
            })
        },
        error: function (error) {
            $('#nameError').text(error.responseJSON.errors.name)
            $('#titleError').text(error.responseJSON.errors.title)
            $('#instituteError').text(error.responseJSON.errors.institute)
        }
    })
})
// =======================================================================================
// ======================== 3. ajax / Edit na data ==============================================
// =======================================================================================

$('#teacher-list').on('click', '.edit-teacher-btn', function (event) {
    var id = $(this).attr('data-teacher-id');

    $.ajax({
        type: "GET",
        dataType: "json",
        url: "/teacher/edit/" + id,
        success: function (data) {
            $('#addT').hide();
            $('#addButton').hide();
            $('#updateT').show();
            $('#updateButton').show();
            $('#id').val(data.id);
            $('#name').val(data.name);
            $('#title').val(data.title);
            $('#institute').val(data.institute);
        }
    })
})
// =======================================================================================
// ======================== 4. ajax / Update na data {zacuvuvanje vo baza} ==============================================
// =======================================================================================

$('#updateButton').on('click', function (e) {
    var id = $('#id').val();
    var name = $('#name').val();
    var title = $('#title').val();
    var institute = $('#institute').val();

    $.ajax({
        type: "POST",
        dataType: "json",
        data: { name: name, title: title, institute: institute },
        url: "/teacher/update/" + id,
        success: function (data) {
            $('#addT').show();
            $('#addButton').show();
            $('#updateT').hide();
            $('#updateButton').hide();
            clearData();
            $('#teacher-list tr').remove()
            allData();
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Uspeshno napraven update',
                showConfirmButton: false,
                timer: 3000
            })
            F
        },
        error: function (error) {
            $('#nameError').text(error.responseJSON.errors.name)
            $('#titleError').text(error.responseJSON.errors.title)
            $('#instituteError').text(error.responseJSON.errors.institute)
        }
    })
})

// =================================================================================================
// =========================== Delete teacher ======================================================
// =================================================================================================

$('#teacher-list').on('click', '.delete-teacher-btn', function (e) {

    var id = $(this).attr('data-teacher-id');
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/teacher/destroy/" + id,
        success: function (data) {

            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Deleted teacher',
                showConfirmButton: false,
                timer: 1500
            })
            clearData();
            $('#teacher-list tr').remove()
            allData();
        }

    })

})



