const { default: Swal } = require('sweetalert2');

require('./bootstrap');

$('#addT').show(); // Da pokazuva add new teacher
$('#addButton').show(); // Da pokazuva add new teacher button
$('#updateT').hide(); // Da go skrie update teacher
$('#updateButton').hide(); // Da go skrie update button

// Setap za CSRF-TOKEN
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})
// =====================================================================================================
// =======================  1.ajax /  Zemanje na podatocite od baza   ========================================
// =======================================================================================================
function allData() {
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: "/teacher/all",
        success: function (response) {
            // Pravime foreach na data, bidejki gi dobivame kako kolekcija.
            // i gi printame vo tabela so id teacher-list
            var teacher = ""
            $.each(response, function (key, teacher) {

                $('#teacher-list tbody').prepend(`
          <tr>
                    <td>${teacher.id}</td>
                    <td>${teacher.name}</td>
                    <td>${teacher.title}</td>
                    <td>${teacher.institute}</td>
                    <td>
                    <button class="btn-sm btn-success edit-teacher-btn" data-teacher-id="${teacher.id }">Edit</button>
                    <button class="btn-sm btn-danger delete-teacher-btn" data-teacher-id="${teacher.id }">Delete</button>
                    </td>
         </tr>
                ` )
            })
        }
    })
}
// Ja povikuvame funkcijata koja ke ni printa teacher
allData();

// =======================================================================================
// ======================== 2. ajax / Add new teacher ===========================================
// =======================================================================================

function clearData() {
    $('#id').val('');
    $('#name').val('');
    $('#title').val('');
    $('#institute').val('');
    // da gi izbrisi i greskata, koga ke e uspesna validacija na nekoj input
    $('#nameError').text('')
    $('#titleError').text('')
    $('#instituteError').text('')
}
// So klik na add, pravime submit na forma
$('#addButton').on('click', function (e) {
    e.preventDefault()
    // vrednosti od input polinjata
    var name = $('#name').val();
    var title = $('#title').val();
    var institute = $('#institute').val();

    // console.log(name,title,institute)
    //  POST na new teacher
    $.ajax({
        type: "POST",
        dataType: "json",
        data: { name: name, title: title, institute: institute },
        url: "/teacher/store/",
        success: function (data) {
            clearData();//brisenje od tabela site vrednosti
            allData();// printanje vo tabela odnovo, a i so novokreiraniot teacher
          // Swal
           const Msg =  Swal.mixin({
              position: 'top-end',
              toast: true,
              icon: 'success',
               showConfirmButton: false,
               timer: 3000
              })
              Msg.fire({
                  type:'success',
                  title: 'Uspeshno dodaden nov user'
              })
        //   Swal.fire({
        //     position: 'top-end',
        //     icon: 'success',
        //     title: 'Dodaden nov teacher',
        //     showConfirmButton: false,
        //     timer: 3000
        //   })
        },
        // Prikazuvanje na greski od validacija
        error: function (error) {
            $('#nameError').text(error.responseJSON.errors.name)
            $('#titleError').text(error.responseJSON.errors.title)
            $('#instituteError').text(error.responseJSON.errors.institute)
        }
    })
})
// =======================================================================================
// ======================== 3. ajax / Edit na data ==============================================
// =======================================================================================

$('#teacher-list').on('click', '.edit-teacher-btn', function(event){
    var id = $(this).attr('data-teacher-id');// Naoganje na id na teacher

    $.ajax({
        type: "GET",
        dataType: "json",
        url: "/teacher/edit/"+id,
        success: function(data){
            $('#addT').hide(); // Da ja sokrie formata za addT
            $('#addButton').hide(); // Da go skrie addButton
            $('#updateT').show(); // Da go pokaze update teacher
            $('#updateButton').show(); // Da go pokaze update button
            // Zemanje na stari vrednosti
            $('#id').val(data.id);
            $('#name').val(data.name);
            $('#title').val(data.title);
            $('#institute').val(data.institute);
            console.log(data)
        }
    })
})
// =======================================================================================
// ======================== 4. ajax / Update na data {zacuvuvanje vo baza} ==============================================
// =======================================================================================
$('#updateButton').on('click', function(e){
    var id = $('#id').val();
    var name = $('#name').val();
    var title = $('#title').val();
    var institute = $('#institute').val();

    $.ajax({
        type: "POST",
        dataType: "json",
        data: { name: name, title: title, institute: institute },
        url: "/teacher/update/"+id,
        success: function(data){
            $('#addT').show(); // Da pokazuva add new teacher
            $('#addButton').show(); // Da pokazuva add new teacher button
            $('#updateT').hide(); // Da go skrie update teacher
            $('#updateButton').hide(); // Da go skrie update button
            clearData();//brisenje od tabela site vrednosti
            allData();// printanje vo tabela odnovo, a i so novokreiraniot teacher
             // Swal (sweetalert)
             Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Uspeshno napraven update',
                showConfirmButton: false,
                timer: 3000
              })
              // End Swal
        },
             // Prikazuvanje na greski od validacija
             error: function (error) {
                $('#nameError').text(error.responseJSON.errors.name)
                $('#titleError').text(error.responseJSON.errors.title)
                $('#instituteError').text(error.responseJSON.errors.institute)
            }
    })
})

// =================================================================================================
// =========================== Delete teacher ======================================================
// =================================================================================================

$('#teacher-list').on('click', '.delete-teacher-btn', function(e){

      var id = $(this).attr('data-teacher-id');// Naoganje na id na teacher
      $.ajax({
          type: "POST",
          dataType: "json",
          url: "/teacher/destroy/"+id,
          success:function(data){

              console.log('deleted')
          }

      })

})



