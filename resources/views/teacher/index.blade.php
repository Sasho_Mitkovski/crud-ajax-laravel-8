<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laravel 8 ajax CRUD app</title>
    <link rel="stylesheet" href="{{ asset('css') }}/app.css">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
</head>

<body>
    <div style="pdding: 30px;"></div>
    <div class="container-fluid">
        <h2 style="color: rgb(83, 15, 161)">
            <marquee behavior="" direction="">Laravel 8 CRUD ajax - Sasho Mitkovski</marquee>
        </h2>
        <div class="row">
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-header">
                        All teacher
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered" id="teacher-list">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Institute</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- <tr>
                                    <td>1</td>
                                    <td>Cacko</td>
                                    <td>Mitko</td>
                                    <td>Music</td>
                                    <td>
                                        <button class="btn btn-success">Edit</button>
                                        <button class="btn btn-danger">Delete</button>
                                    </td>
                                </tr> --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header">
                        <span id="addT">Add new teacher</span>
                        <span id="updateT">Update teacher</span>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name" class="form-label">Name</label>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Enter Name"
                                aria-describedby="emailHelp">
                                {{-- Printanje na geski --}}
                                <span class="text-danger" id="nameError"></span>
                        </div>
                        <div class="form-group">
                            <label for="title" class="form-label">Title</label>
                            <input type="text" name="title" id="title" class="form-control" placeholder="Job position"
                                aria-describedby="emailHelp">
                                <span class="text-danger" id="titleError"></span>
                        </div>
                        <div class="form-group">
                            <label for="institute" class="form-label">Institute name</label>
                            <input type="text" name="institute" id="institute" class="form-control" placeholder="Institute Name"
                                aria-describedby="emailHelp">
                                <span class="text-danger" id="instituteError"></span>
                        </div>
                        {{-- Ke ni treba hidden pole za update i delete --}}
                        <input type="hidden" id="id">
                        <button type="submit" id="addButton" class="btn-sm btn-primary">Add</button>
                        <button type="submit" id="updateButton" class="btn-sm btn-primary">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
