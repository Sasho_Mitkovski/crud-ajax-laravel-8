<?php

use App\Http\Controllers\TeacherController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('teacher/index');
});

Auth::routes();
//                    CRUD: ajax
Route::get('/ajax', [TeacherController::class, 'index']);
Route::get('/teacher/all', [TeacherController::class, 'allData']);//ajax 1
Route::post('/teacher/store/', [TeacherController::class, 'storeData']);//ajax 2
Route::get('/teacher/edit/{id}', [TeacherController::class, 'editData']);//ajax 3
Route::post('/teacher/update/{id}', [TeacherController::class, 'updateData']);//ajax 3
Route::post('/teacher/destroy/{id}', [TeacherController::class, 'destroyData']);//ajax 3
